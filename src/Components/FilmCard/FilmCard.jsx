import React from 'react';
import './styles.css';

const FilmCard = ({
  title, popularity, movieImage, genres,
}) => {
  const genreSentence = genres.length === 1 ? genres[0] : `${genres.slice(0, genres.length - 1).join(', ')} and ${genres.slice(-1)}`;

  return (
    <div className="movie-card">
      <img src={`https://image.tmdb.org/t/p/w500/${movieImage}`} alt="movie name" />
      <h2>{title}</h2>
      <p>{popularity}</p>
      <p>{genreSentence}</p>
    </div>
  );
};

export default FilmCard;
