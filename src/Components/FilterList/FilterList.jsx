import React from 'react';
import './styles.css';

const FilterList = ({
  genres, filters, handleChange, handleReset,
}) => (
  <div>
    <div className="filter-list">
      {genres.map((genre, i) => (
        <div key={i} className="filter">
          <p>{genre.name}</p>
          <input type="checkbox" name={genre.name} id={genre.id} checked={filters.includes(genre.id)} onChange={(e) => handleChange(e)} />
        </div>
      ))}
    </div>
    <div className="reset-button">
      <button type="button" onClick={handleReset}>Reset Filters</button>
    </div>
  </div>
);

export default FilterList;
