import React, { useState, useEffect } from 'react';
import { fetchMovies } from './api/movies';
import { fetchGenres } from './api/genres';
import FilterList from './Components/FilterList';
import FilmCard from './Components/FilmCard';
import './styles.css'; // check this file out & feel free to use the classes

export default function App() {
  const [movies, setMovies] = useState([]);
  const [genres, setGenres] = useState([]);
  const [filters, setFilters] = useState([]);

  useEffect(() => {
    fetchMovies().then((res) => res.json().then((data) => setMovies(data)));
    fetchGenres().then((res) => res.json().then((data) => setGenres(data)));
  }, []);

  const genreFilter = (arr) => {
    if (filters.length === 0) return arr;
    return filters.every((item) => arr.genre_ids.includes(item));
  };

  const filteredMovies = movies.sort().filter(genreFilter);

  const createMovieGenreArr = (genreIds) => genres.filter((item) => genreIds.includes(item.id)).map((filterdItem) => filterdItem.name);

  const handleChange = (e) => {
    if (e.target.checked) {
      setFilters([...filters, Number(e.target.id)]);
    } else {
      setFilters(filters.filter((item) => item !== Number(e.target.id)));
    }
  };

  const handleReset = () => setFilters([]);

  return (
    <div>
      <h1>
        <span>
          <span role="img" aria-label="Popcorn emoji">
            🍿
          </span>
          {' '}
          Now playing
        </span>
      </h1>
      <h2>
        Showing
        {' '}
        {filteredMovies.length}
        {' '}
        movies
      </h2>
      <FilterList
        genres={genres}
        filters={filters}
        handleChange={handleChange}
        handleReset={handleReset}
      />
      <div className="movie-container">
        {filteredMovies.map((movie, i) => (
          <FilmCard
            key={i}
            title={movie.title}
            popularity={movie.popularity}
            movieImage={movie.poster_path}
            genres={createMovieGenreArr(movie.genre_ids)}
          />
        ))}
      </div>
    </div>
  );
}
